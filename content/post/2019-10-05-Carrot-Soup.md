---
title: Modernist Simplified Carrot Soup
subtitle: Very good, but very unusual
date: 2019-10-05
tags: ["soup", "pressure cooker", "modern"]
---

A simplified modernist cuisine recipe for carrot soup. Cause not everyone
has access to a centrifuge and this recipe will get you 95% same results
with only 50% of the work. 

By cooking in a pressure cooker with baking soda the PH is lowered and temperature is higher.
This combo allows the sugars in the carrots
to carmelize to an amazing almost cotton candy smell.

# Ingrediants

Ingredient | Measurement, Weight | Notes
---|---|----
Carrots | 750 Grams | peeled, quartered, cut into 1inch pieces
Butter | 210 Grams | Salted is fine, unsalted probably is too
Salt | 11.25 Grams | 
Baking Soda | 3.75 Grams | 
Carrot Juice | 952 Graams | I've had luck with bolthouse farms

# Method

1. Place butter and then carrots in pressure cooker, turn on heat.
1. Add salt and sprinkle baking soda over top, stir in.
1. I usually add a splash of water for the pressure cooker but no more than 1/4 cup. 
1. Cook 20 minutes at high pressure.
1. Quick release with cold water on the pressure cooker.
1. Pour in the carrot juice and use a stick blender until smooth.

You can add things like toasted coriander or coconut cream foam, which are good. 
But, honestly these sometimes take away from the intense carroty goodness and
focus of the soup.
